var express = require("express");
var router = express.Router();

router.get("/:num", function (req, res, next) {
  if (isNaN(req.params.num)) {
    res.sendStatus(400);
  }
  var output = { dados: [] };
  for (let i = 0; i < req.params.num; i++) {
    output.dados.push(Math.floor(Math.random() * 6) + 1);
  }
  res.send(output);
});

module.exports = router;
